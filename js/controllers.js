//listPage's Controller
app.controller('ListController', function($scope, $location, usersFactory){

    $scope.users = usersFactory.getUserList();
    // use for pagination
    $scope.currentPage = 0;
    $scope.pageSize = 5; 
    $scope.numberOfPages=function(){
        return Math.ceil($scope.users.length/$scope.pageSize);                
    };

    // use for order by column name
    $scope.orderBy = function(colName) {
        $scope.myOrderBy = colName;
    };

    // for each edit button
    $scope.goEditPage = function(path) {
        $location.path(path);

    }

    // for delete button
    $scope.deleteUser = function(user) {
        usersFactory.deleteUser(user);
    };

});

// for pagination
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

// New page's Controller
app.controller('NewController',function($scope, $routeParams, $location, usersFactory){
    // define variables
    //$scope.users = usersFactory.getUserList();
    $scope.id = '';
    $scope.fName = '';
    $scope.lName = '';
    $scope.title = '';
    $scope.sex = '';
    $scope.age = '';
    $scope.passw1 = '';
    $scope.passw2 = '';

    //input check flags
    $scope.edit = true;
    $scope.error = false;
    $scope.incomplete = false; 

    $scope.addUser = function(){
        $scope.id = usersFactory.getMaxId() + 1;
        // save user button
        var newUser = {
            id:$scope.id, 
            fName:$scope.fName, 
            lName:$scope.lName, 
            title:$scope.title, 
            sex:$scope.sex, 
            age:parseInt($scope.age) 
        };

        usersFactory.addNewUser(newUser);
        //route to the list
        $location.path('listPage');
    };

    // Copy from w3c
    $scope.$watch('passw1',function() {$scope.test();});
    $scope.$watch('passw2',function() {$scope.test();});
    $scope.$watch('fName', function() {$scope.test();});
    $scope.$watch('lName', function() {$scope.test();});

    $scope.test = function() {
        if ($scope.passw1 !== $scope.passw2) {
            $scope.error = true;
        } else {
            $scope.error = false;
        }
        $scope.incomplete = false;
        if ($scope.edit && (!$scope.fName.length ||!$scope.lName.length ||!$scope.passw1.length || !$scope.passw2.length)) {
            $scope.incomplete = true;
        }
    };

});

// Edit page's Controller
app.controller('EditController',function($scope, $routeParams, $location, usersFactory){
    // define variables
    //TODO $routeParams.param error
    $scope.id = $routeParams.param;
    $scope.user = usersFactory.getUserById($scope.id);
    $scope.fName = $scope.user.fName;
    $scope.lName = $scope.user.lName;
    $scope.title = $scope.user.title;
    $scope.sex = $scope.user.sex;
    $scope.age = $scope.user.age; 

    //input check flags
    $scope.edit = false;
    $scope.error = false;
    $scope.incomplete = false; 

    $scope.updateUser = function(id){
        usersFactory.updateUser(id, $scope.fName, $scope.lName, $scope.title, $scope.sex, parseInt($scope.age));
        //route to the list
        $location.path('listPage');
    }

    // Copy from w3c
    $scope.$watch('passw1',function() {$scope.test();});
    $scope.$watch('passw2',function() {$scope.test();});
    $scope.$watch('fName', function() {$scope.test();});
    $scope.$watch('lName', function() {$scope.test();});

    $scope.test = function() {
        if ($scope.passw1 !== $scope.passw2) {
            $scope.error = true;
        } else {
            $scope.error = false;
        }
        $scope.incomplete = false;
        if ($scope.edit && (!$scope.fName.length ||!$scope.lName.length ||!$scope.passw1.length || !$scope.passw2.length)) {
            $scope.incomplete = true;
        }
    };

});

