var app = angular.module('routeApp', ['ngRoute']);

// set config for route path 
app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'listPage.html',
                controller: 'ListController'
            }).
            when('/newPage/:param', {
                templateUrl: 'newPage.html',
                controller: 'NewController'
            }).
            when('/editPage/:param', {
                templateUrl: 'editPage.html',
                controller: 'EditController'
            }).
            otherwise({
                redirectTo: '/'
            });
}]);