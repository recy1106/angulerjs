﻿// use factory for data storage
app.factory('usersFactory', function(){
    var usersList = [
        {id:1, fName:'Hege', lName:"Pege", title:'manager', sex:'m', age:36 },
        {id:2, fName:'Kim',  lName:"Pim", title:'salesman', sex:'m', age:30 },
        {id:3, fName:'Sal',  lName:"Smith", title:'salesman', sex:'m', age:26 },
        {id:4, fName:'Jack', lName:"Jones", title:'salesman', sex:'m', age:32 },
        {id:5, fName:'John', lName:"Doe", title:'salesman', sex:'m', age:28 },
        {id:6, fName:'Peter',lName:"Pan", title:'salesman', sex:'m', age:25 },
        {id:7, fName:'Ge', lName:"He", title:'manager', sex:'m', age:33 },
        {id:8, fName:'Kelly',  lName:"Pim", title:'salesman', sex:'f', age:31 },
        {id:9, fName:'Sally',  lName:"Smith", title:'salesman', sex:'f', age:25 },
        {id:10, fName:'Jacky', lName:"Jones", title:'salesman', sex:'m', age:33 },
        {id:11, fName:'Joanna', lName:"Doe", title:'salesman', sex:'f', age:29 },
        {id:12, fName:'Pei',lName:"Pan", title:'salesman', sex:'m', age:27 }
        ];

    return {
        getUserList: function() {
            return usersList; },
        
        getUserById: function(inputId) {
            var idx = -1;
            for (var i=0; i<usersList.length; i++) {
                if (inputId == usersList[i].id) {
                    //save index of the changing usersList
                    idx = i;
                }
            }
            return usersList[idx]; },
        
        addNewUser: function(user) {
            usersList.push(user); 
            return usersList;},

        updateUser: function(id, fName, lName, title, sex, age){
            var idx = -1;
            // find new index from the changing usersList
            for (var i=0; i<usersList.length; i++) {
                if (id == usersList[i].id) {
                  idx = i;
                }
            }
            usersList[idx].fName = fName;
            usersList[idx].lName = lName;
            usersList[idx].title = title;
            usersList[idx].sex = sex;
            usersList[idx].age = age; 
            return usersList; },

        deleteUser: function(user) {
            var idx = usersList.indexOf(user);
            usersList.splice(idx, 1);
            return usersList; },

        getMaxId: function() {
            var id = -1;
            for (var i=0; i<usersList.length; i++) {
                if (id < usersList[i].id) {
                  id = usersList[i].id;
                }
            }
            return id; }
    };
});