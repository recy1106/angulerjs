var express = require('express');
var router = express.Router();

// for MongoDB connection
var mongoose = require('mongoose');

//mongodb://username:pwd@hostname/dbname
mongoose.connect('mongodb://localhost:27017/local');

// for using user model
var User = require('../app/models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log('Welcome to use userList API.');
	//res.send('respond with a resource');
});

/* GET users listing. */
router.get('/userlist', function(req, res, next) {
	console.log("Request handler 'GET /userlist' was called. ");

  User.find(function(err, userlist) {
    if (err)
      res.send(err);

    console.log("'GET /userlist' from MongoDB has been sent back. ");
    res.json(userlist);
  });

});

/* POST to add a new user in users list. */
router.post('/userlist', function(req, res, next) {
	console.log("Request handler 'POST /userlist' was called. ");
	console.log("req.body info:  " + JSON.stringify(req.body));

  // find the max_id from db
  User.find(function(err, userlist) {
    if (err)
      res.send(err);

    //var max_id = getMaxId(userlist);
    var new_id = 1 + getMaxId(userlist);
    console.log("user with new_id:  " + new_id);
    var v_fname = req.body.fName;
    var v_lname = req.body.lName;
    var v_title = req.body.title;
    var v_sex = req.body.sex;
    var v_age = parseInt(req.body.age);

    var user  = new User();
    user._id = new_id;
    user.fName = v_fname;
    user.lName = v_lname;
    user.title = v_title;
    user.sex = v_sex;
    user.age = v_age;

    // save the user and check for errors
    user.save(function(err) {
      if (err)
        res.send(err);

      // send whole list back to page
      res.json({ message: 'User added successfully!' }); 
    });

    function getMaxId(usersList) {
        var id = -1;
        for (var i=0; i<usersList.length; i++) {
            if (id < parseInt(usersList[i].id)) {
                id = parseInt(usersList[i].id);
            }
        }
        console.log("getMaxId = "+ id);
        return id; 
    }

  });

});

/* GET to read a user by the user's id. */
router.get('/userlist/:id', function(req, res, next) {
	console.log("Request handler 'GET /userlist/:id="+req.params.id+" was called. ");
	
  User.findById(req.params.id, function(err, user) {
    if (err)
      res.send(err);

    // send the user back
    res.json(user); 
  });
 
});

/* PUT to update a user by the user's id. */
router.put('/userlist/:id', function(req, res, next) {
	console.log("Request handler 'PUT /userlist/:id="+req.params.id+" was called. ");

  // use our User model to find the user by id
  User.findById(req.params.id, function(err, user) {
    if (err)
      res.send(err);

    // update the user's info
    user.fName = req.body.fName;
    user.lName = req.body.lName;
    user.title = req.body.title;
    user.sex = req.body.sex;
    user.age = parseInt(req.body.age);

    // save the user
    user.save(function(err) {
      if (err)
      res.send(err);
        res.json({ message: 'User updated successfully!' });
    });

  });

});

/* DELETE to delete a user by the user's id. */
router.delete('/userlist/:id', function(req, res, next) {
	console.log("Request handler 'DELETE /userlist/:id="+req.params.id+" was called. ");
	
  User.remove({
    _id: req.params.id
  }, function(err, user) {
      if (err)
          res.send(err);

      res.json({ message: 'User deleted successfully!' });
  });

});

module.exports = router;
