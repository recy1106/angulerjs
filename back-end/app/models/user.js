// app/models/user.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//_id is the primary key
var UserSchema = new Schema({
	_id: Number, 
	fName: String,
	lName: String,
	title: String,
	sex: String,
	age: Number
},{
	collection: 'user'
});

module.exports = mongoose.model('User', UserSchema);